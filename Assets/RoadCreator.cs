﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadEditor1 : MonoBehaviour {

    [HideInInspector]
    public Path path;

    public void CreatePath()
    {
        path = new Path(transform.position);
    }
}
